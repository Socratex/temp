Hi :)

This is a project made for Codibly by Michał Jasiński.<br>

The analysis start point is:<br>
\App\ProductBundle\Infrastructure\Adapter\ProductController

The specification of the task was very vague so I figured that this should be interpreted as the "problem to solve".<br>
I started by creating ADR that helped me with quite abstract decision:<br>
"Assuming that we don't know anything about the development of the project - how should we implement the 'Product'?"<br>
Of course I could just do controller-listener-service "YAGNI" solution, but:<br>

- I wouldn't learn much (I've no practical experience in DDD or hexagonal-onion)
- the code would be "correct", but it wouldn't be something to discuss about

Nonetheless, I failed by spending too much time struggling with following configurations:

- composer errors (missing php addon) - fixed locally
- setting up Symfony skeleton (http returning 403) - initialized project differently
- gitlab repository initialization issues (I needed to re-learn ssh keys gitlab config)
- failing namespace recognition (I suspect error in composer.json) leading to e.g. PHPStorm not recognizing routes
