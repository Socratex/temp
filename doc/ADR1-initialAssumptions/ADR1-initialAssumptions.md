## Initial analysis of the project request

First assumption:<br>
This is not a "production" app, but a recruitment test one.<br>
Hence, I will not be asking for additional information.<br>
I will try to create "future-proof" code and "expect the unexpected".<br>
I assume that I don't need to create additional problems to prove myself.<br>

## Solution propositions

Potential solution modules* :

1. NONE - simple "write code as-is" solution
1. DDD (+ADI) - create 3 layers - application, domain, infrastructure
1. ES - use event sourcing
1. CQRS - separate command and query data and mechanisms
1. "CFT" (not an official acronym) - create Codeception Feature Tests (functional)

*All modules (apart from "NONE" can be used simultaneously).

## Solution propositions explanations

**NONE**<br>
Create a controller that translates REST calls to event data.<br>
Create service that handles messages from message bus.<br>
For example service can use repository interface to save product directly from data acquired from message bus.<br>

**DDD**<br>
Look at code from the perspective of domains.<br>
Create "Bounded Context" - an area (for example - a Bundle) in which entities would have the same meaning.<br>
Guard its borders strictly (no Bounded Context should have dependency on other).<br>
They should behave like API even if they could be referenced directly.

This would help maintain the order in the scope of the domain.<br>
Also, scaling projects designed in such manner by extracting microservices would greatly decrease the API calls count and weight.<br>
(since all the data needed for operating in a single domain would be in that domain)

ADI: There will be 3 namespaces for each domain:

- domain - business logic, completely unaware of other namespaces (lack of dependency is a requirement here)
- application - controlling the flow of application using infrastructure interfaces and domain logic
- infrastructure - calls to the infrastructure such as repositories or queues, adapters such as controllers and commands

This would help in... separation of those 3 layers :D<br>
(lots of various perks from exchangeable infrastructure to easier domain management)

**ES**<br>
Persisted entities would be in form of a list of events that led to their creation.<br>
For performance, there could be an additional data source that saves result snapshot of the data created by those events.<br>
This would potentially allow us to recreate every event that led to current state of persisted data.<br>

**CQRS**<br>
Command and Query will have separate data models, no matter how similar they would be.<br>
Command is a request for change, Query is a request for data.<br>
If we have two natures of requests then we have two groups of drivers to consider.<br>
If we have two groups of drivers - two different sets of solutions should be used.<br>

Easiest example is saving product and reading list of products.<br>
We ensure correct state of saved data -> for instance - rich data model, validators, maybe saved-by-aggregate with additional policies, possibly with some external API calls...<br>
We assume correct state of read data so none of the above is required nor recommended (because of performance - for example - "ORMs have poor performance in batch operations").<br>

**CFT**<br>
Functional tests would be in form of behavioral configurations that would be human-readable.<br>
Tests would be easier to write and understand -> there will be greater functional coverage which is essential for regression.

## Solution-Drivers comparison:

Considered drivers:

1. maintainability/elasticity
2. auditability/diagnostics
3. readability/complexity
4. testability
5. potential risks
6. Additional perks/"clean code" factor*
7. time cost (because it's a short-term project)**
8. potential implementation obstacles (because it's a short-term projects)**
9. "I'll learn something" factor (because again - it's not a real production project)***
10. Additional perks

**Comparison table** (assuming proper implementation):

| | NONE | DDD+ADI | ES | CQRS | CFT |
| :--- | :--- | :--- | :--- | :--- | :--- |
|1|No perks|Easy to replace Infrastructure.<br>Aggregates = easier operations.|Versioning can be a pain|Very elastic, each model can be changed separately|Lots of work, but gets the testing job done.|
|2|No perks|Easy per-layer error check|No loss of state of data flow info.<br>Everything is auditable.<br>Great case replication potential.|Easier per-flow diagnostics.<br>Separate data models = easy additional data.|Good regression makes proving that something works as intended possible.|
|3|No perks|Separated layers makes each of them easier to understand.|Can make code harder to "follow" and persistence more complicated.|Helps focusing on single flow when reading code, no redundant data in model.|Each tested flow is very easy to read|
|4|No perks|Unit tests can focus on domain layer only|Can test sequence of events leading to "entity" creation|Easier flow testing (easier setup for each data model).|Full feature testing, easy TDD, great method for regression.|
|5|Potential mudball|None.<br>Moreover, proper domain separation prevents issues associated with microservices (poorly defined domain borders + scaling = API nightmare).|Bloated DB|Data model errors are more "local".<br>And that makes their detection harder.|None|
|6|None|modular monolith approach + DDD approach = great potential of properly defined bounded context => MUCH less scalability issues<br>Moreover, separation of the domain layer is clean|More event-oriented code|No multitasking data models => less possibilities to break principles.<br>Potential BIG performance improve|Enforces TDD, feature requests could be in form of tests in some cases|
|7|VERY fast to implement|Needs more models and interfaces, but tests are easier to write|Needs persistence of every event and translation to change of entity.|Needs more models, but they're very intuitive|Needs proper configuration (and of course - written test cases)|
|8|None|None|Lack of time|None|Configuration issues|
|9|None|I only know DDD in theory<br>Nonetheless, project of this complexity won't help me gain experience.|I only know ES in theory.<br>This project could help me gain little experience.|Probably won't learn anything new in this scale.|I would learn how to configure what I'm using on daily basis.|

*note - "clean code" factor is about references to good practices such as "single responsibility"<br>
we can argue about it's application on different layers and discuss about abstract interpretations of such rules<br>
we can discuss about consequences of being too strict with them or consequences of breaking them<br>
but at the same time we all know it's a valid argument to consider in discussion and see some level of authority in their names<br>
that's because they resonate with all of our experiences - and they're worth a consideration

**note - time cost and potential implementation obstacles also a valid driver in production ADRs<br>
but it's not applicable here, even if this was a production project (because of lack of context)

***note - some technologies I know only in theory and since it's only a recruitment project without production repercussions<br>
I can as well learn something from it ;)

## Discussion

N/A, I don't have a team to argue with (since this is my recruitment project) :D

## Decision

I'll try to implement the ADI + CQRS + CFT solutions.<br>

ADI, CQRS and CFT (imho) should be used "by default".<br>
There should be arguments against (which I found none in this case) when considering not-applying them.

ES - I have no time to implement it now. Nonetheless, it is great especially for such events as editing products (overall - for implementing orders/products/stocks)
It's also great for logging events.
