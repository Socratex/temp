<?php

namespace App\ProductBundle\Infrastructure\Repository;

use App\ProductBundle\Application\Repository\ProductRepositoryInterface;

class InMemoryProductRepository implements ProductRepositoryInterface
{
}
