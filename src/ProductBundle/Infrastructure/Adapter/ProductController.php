<?php

namespace App\ProductBundle\Infrastructure\Adapter;

use App\ProductBundle\Application\Service\ProductService;
use App\ProductBundle\Infrastructure\Listener\CreateProductListener;
use App\ProductBundle\Infrastructure\Listener\EditProductListener;
use App\ProductBundle\Infrastructure\Service\ProductDataExtractorService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class ProductController extends AbstractController
{
    /** @var ProductDataExtractorService */
    private $productDataExtractor;

    public function __construct()
    {
        $this->productDataExtractor = $this->get(ProductDataExtractorService::class);
    }

    public function productsListAction(): Response
    {
        return $this->render('@Product/product/list_view.html.twig');
    }

    public function filterProductsListAjaxAction(Request $request): Response
    {
        $productsListData = null;
        try {
            $productService = $this->get(ProductService::class);

            $requestData = $this->getRequestData($request);
            $productsListData = $this->productDataExtractor->getProductsListData($requestData);

            $productService->getProducts($productsListData);
        } catch (\Throwable $e) {
            return new JsonResponse(['error' => $e->getMessage()], Response::HTTP_INTERNAL_SERVER_ERROR);
        }

        return new JsonResponse(['products' => $productsListData], Response::HTTP_OK);
    }

    public function viewProductAction(int $id): Response
    {
        $productService = $this->get(ProductService::class);
        $product = $productService->getProduct();

        return $this->render('ProductBundle:product:product_view.html.twig', ['product' => $product]);
    }

    public function createProductAjaxAction(Request $request): JsonResponse
    {
        try {
            $requestData = $this->getRequestData($request);
            $createProductData = $this->productDataExtractor->getCreateProductData($requestData);
            /** @see CreateProductListener::__invoke */
            $this->dispatchMessage($createProductData);
        } catch (\Throwable $e) {
            return new JsonResponse(["error" => $e->getMessage()], Response::HTTP_INTERNAL_SERVER_ERROR);
        }

        return new JsonResponse([""], Response::HTTP_OK);
    }

    public function editProductAjaxAction(Request $request): Response
    {
        try {
            $requestData = $this->getRequestData($request);
            $editProductData = $this->productDataExtractor->getEditProductData($requestData);
            /** @see EditProductListener::__invoke */
            $this->dispatchMessage($editProductData);
        } catch (\Throwable $e) {
            return new JsonResponse(['error' => $e->getMessage()], Response::HTTP_INTERNAL_SERVER_ERROR);
        }

        return new JsonResponse([''], Response::HTTP_OK);
    }

    private function getRequestData(Request $request): array
    {
        return json_decode($request->getContent(), true);
    }
}
