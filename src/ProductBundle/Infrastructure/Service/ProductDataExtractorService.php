<?php

namespace App\ProductBundle\Infrastructure\Service;

use App\ProductBundle\Domain\Data\Builder\ProductBuilder;
use App\ProductBundle\Domain\Data\Command\CreateProductData;
use App\ProductBundle\Domain\Data\Command\EditProductData;
use App\ProductBundle\Domain\Data\Product;
use App\ProductBundle\Domain\Data\Query\ProductsListData;

class ProductDataExtractorService
{
    public function getCreateProductData(array $source): CreateProductData
    {
        //TODO socratex - FINISH ME - extract object from request data
        return new CreateProductData();
    }

    public function getEditProductData(array $source): EditProductData
    {
        //TODO socratex - FINISH ME - extract object from request data
        return new EditProductData();
    }

    public function getProductsListData(array $source): ProductsListData
    {
        //TODO socratex - FINISH ME - extract object from request data
        return new ProductsListData();
    }

    public function getProductData(array $source): Product
    {
        //TODO socratex - FINISH ME - extract object from request data
        return ProductBuilder::create()->build();
    }
}
