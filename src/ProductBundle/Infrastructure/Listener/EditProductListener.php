<?php

namespace App\ProductBundle\Infrastructure\Listener;

use App\ProductBundle\Application\Service\ProductService;
use App\ProductBundle\Domain\Data\Command\EditProductData;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;

class EditProductListener implements MessageHandlerInterface
{
    /** @var ProductService */
    private $productService;

    public function __construct(ProductService $productService)
    {
        $this->productService = $productService;
    }

    public function __invoke(EditProductData $data)
    {
        $this->productService->editProduct($data);
    }
}
