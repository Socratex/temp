<?php

namespace App\ProductBundle\Infrastructure\Listener;

use App\ProductBundle\Application\Service\ProductService;
use App\ProductBundle\Domain\Data\Command\CreateProductData;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;

class CreateProductListener implements MessageHandlerInterface
{
    /** @var ProductService */
    private $productService;

    public function __construct(ProductService $productService)
    {
        $this->productService = $productService;
    }

    public function __invoke(CreateProductData $data)
    {
        $this->productService->createProduct($data);
    }
}
