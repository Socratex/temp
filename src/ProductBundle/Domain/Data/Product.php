<?php

namespace App\ProductBundle\Domain\Data;

use Ramsey\Uuid\UuidInterface;

class GetProductData
{
    /** @var UuidInterface|null */
    private $id;

    /** @var float */
    private $price;

    /** @var string */
    private $name;

    public function __construct(
        ?UuidInterface $id,
        float $price,
        string $name
    ) {
        $this->id = $id;
        $this->price = $price;
        $this->name = $name;
    }

    public function getId(): ?UuidInterface
    {
        return $this->id;
    }

    public function getPrice(): float
    {
        return $this->price;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function serialize()
    {
        // TODO: Implement serialize() method.
    }

    public function unserialize($serialized)
    {
        // TODO: Implement unserialize() method.
    }
}
