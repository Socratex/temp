<?php

namespace App\ProductBundle\Domain\Data\Command;

use Ramsey\Uuid\UuidInterface;

class EditProductData extends CreateProductData
{
    /** @var UuidInterface */
    private $productId;
}
