<?php

namespace App\ProductBundle\Domain\Data\Builder;

interface BuilderInterface
{
    /** @return $this */
    public static function create();

    public function build();
}
