<?php

namespace App\ProductBundle\Domain\Data\Builder;

use App\ProductBundle\Domain\Data\Product;

class ProductBuilder implements BuilderInterface
{
    public static function create(): self
    {
        return new self();
    }

    public function build(): Product
    {
        return new Product();
    }
}
