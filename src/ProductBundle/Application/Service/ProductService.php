<?php

namespace App\ProductBundle\Application\Service;

use App\ProductBundle\Application\Repository\ProductRepositoryInterface;
use App\ProductBundle\Domain\Data\Command\CreateProductData;
use App\ProductBundle\Domain\Data\Command\EditProductData;
use App\ProductBundle\Domain\Data\Product;

class ProductService
{
    /** @var ProductRepositoryInterface */
    private $productRepo;

    public function getProducts(Product $getProductData): void
    {
    }

    public function editProduct(EditProductData $data): void
    {
    }

    public function createProduct(CreateProductData $data): void
    {
    }
}
